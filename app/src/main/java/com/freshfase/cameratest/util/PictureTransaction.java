package com.freshfase.cameratest.util;

import android.hardware.Camera;

/**
 * Created by durai on 29/5/16.
 */
public class PictureTransaction implements Camera.ShutterCallback {
  CameraHost host=null;
  boolean needBitmap=false;
  boolean needByteArray=true;
  private Object tag=null;
  boolean mirrorFFC=false;
  boolean useSingleShotMode=false;
  int displayOrientation=0;
  String flashMode=null;
  CameraView cameraView=null;

  public PictureTransaction(CameraHost host) {
    this.host=host;
  }

  public PictureTransaction needBitmap(boolean needBitmap) {
    this.needBitmap=needBitmap;

    return(this);
  }

  public PictureTransaction needByteArray(boolean needByteArray) {
    this.needByteArray=needByteArray;

    return(this);
  }

  boolean useSingleShotMode() {
    return(useSingleShotMode || host.useSingleShotMode());
  }

  boolean mirrorFFC() {
    return(mirrorFFC || host.mirrorFFC());
  }


  public PictureTransaction flashMode(String flashMode) {
    this.flashMode=flashMode;

    return(this);
  }

  @Override
  public void onShutter() {
    Camera.ShutterCallback cb=host.getShutterCallback();
    if (cb != null) {
      cb.onShutter();
    }
  }
}
