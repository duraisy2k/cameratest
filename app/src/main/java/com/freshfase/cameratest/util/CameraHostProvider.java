package com.freshfase.cameratest.util;


/**
 * Created by durai on 29/5/16.
 */
public interface CameraHostProvider {
  CameraHost getCameraHost();
}
