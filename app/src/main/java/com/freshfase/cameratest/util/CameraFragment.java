package com.freshfase.cameratest.util;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by durai on 29/5/16.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CameraFragment extends Fragment {
    private CameraView cameraView = null;
    private CameraHost host = null;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        cameraView = new CameraView(getActivity());
        cameraView.setHost(getCameraHost());

        return (cameraView);
    }

    @Override
    public void onResume() {
        super.onResume();

        cameraView.onResume();
    }

    @Override
    public void onPause() {
        cameraView.onPause();
        super.onPause();
    }

    public CameraHost getCameraHost() {
        if (host == null) {
            host = new SimpleCameraHost(getActivity());
        }

        return (host);
    }

    public void setCameraHost(CameraHost host) {
        this.host = host;
    }

    public void takePicture() {
        takePicture(false, true);
    }

    public void takePicture(boolean needBitmap, boolean needByteArray) {
        cameraView.takePicture(needBitmap, needByteArray);
    }

    public void takePicture(PictureTransaction xact) {
        cameraView.takePicture(xact);
    }

    public void lockToLandscape(boolean enable) {
        cameraView.lockToLandscape(enable);
    }

    public void autoFocus() {
        cameraView.autoFocus();
    }

    public ZoomTransaction zoomTo(int level) {
        return (cameraView.zoomTo(level));
    }

    public void startFaceDetection() {
        cameraView.startFaceDetection();
    }

    public void stopFaceDetection() {
        cameraView.stopFaceDetection();
    }

    public boolean doesZoomReallyWork() {
        return (cameraView.doesZoomReallyWork());
    }

    public void setFlashMode(String mode) {
        cameraView.setFlashMode(mode);
    }
}
