package com.freshfase.cameratest.util;

import android.hardware.Camera;
import android.hardware.Camera.OnZoomChangeListener;

/**
 * Created by durai on 29/5/16.
 */
final public class ZoomTransaction implements OnZoomChangeListener {
    private Camera camera;
    private int level;
    private Runnable onComplete = null;
    private OnZoomChangeListener onChange = null;

    ZoomTransaction(Camera camera, int level) {
        this.camera = camera;
        this.level = level;
    }

    public ZoomTransaction onComplete(Runnable onComplete) {
        this.onComplete = onComplete;

        return (this);
    }

    public void go() {
        Camera.Parameters params = camera.getParameters();

        if (params.isSmoothZoomSupported()) {
            camera.setZoomChangeListener(this);
            camera.startSmoothZoom(level);
        } else {
            params.setZoom(level);
            camera.setParameters(params);
            onZoomChange(level, true, camera);
        }
    }

    @Override
    public void onZoomChange(int zoomValue, boolean stopped, Camera camera) {
        if (onChange != null) {
            onChange.onZoomChange(zoomValue, stopped, camera);
        }

        if (stopped && onComplete != null) {
            onComplete.run();
        }
    }
}
