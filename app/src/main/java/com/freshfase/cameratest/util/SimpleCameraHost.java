package com.freshfase.cameratest.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.Camera;
import android.media.MediaActionSound;
import android.media.MediaScannerConnection;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by durai on 29/5/16.
 */
public class SimpleCameraHost implements CameraHost {
  private static final String[] SCAN_TYPES= { "image/jpeg" };
  private Context ctxt=null;
  private int cameraId=-1;
  private DeviceProfile profile=null;
  private File photoDirectory=null;
  private boolean mirrorFFC=false;
  private boolean useFrontFacingCamera=false;
  private boolean scanSavedImage=true;
  private boolean useFullBleedPreview=true;
  private boolean useSingleShotMode=false;

  public SimpleCameraHost(Context _ctxt) {
    this.ctxt=_ctxt.getApplicationContext();
  }

  @Override
  public Camera.Parameters adjustPictureParameters(PictureTransaction xact,
                                                   Camera.Parameters parameters) {
    return(parameters);
  }

  @Override
  public Camera.Parameters adjustPreviewParameters(Camera.Parameters parameters) {
    return(parameters);
  }

  @Override
  public int getCameraId() {
    if (cameraId == -1) {
      initCameraId();
    }

    return(cameraId);
  }

  private void initCameraId() {
    int count=Camera.getNumberOfCameras();
    int result=-1;

    if (count > 0) {
      result=0; // if we have a camera, default to this one

      Camera.CameraInfo info=new Camera.CameraInfo();

      for (int i=0; i < count; i++) {
        Camera.getCameraInfo(i, info);

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK
            && !useFrontFacingCamera()) {
          result=i;
          break;
        }
        else if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT
            && useFrontFacingCamera()) {
          result=i;
          break;
        }
      }
    }

    cameraId=result;
  }

  @Override
  public DeviceProfile getDeviceProfile() {
    if (profile == null) {
      initDeviceProfile(ctxt);
    }

    return(profile);
  }

  private void initDeviceProfile(Context ctxt) {
    profile=DeviceProfile.getInstance(ctxt);
  }

  @Override
  public Camera.Size getPictureSize(PictureTransaction xact,
                                    Camera.Parameters parameters) {
    return(CameraUtils.getLargestPictureSize(this, parameters));
  }

  @Override
  public Camera.Size getPreviewSize(int displayOrientation, int width,
                                    int height,
                                    Camera.Parameters parameters) {
    return(CameraUtils.getBestAspectPreviewSize(displayOrientation,
                                                width, height,
                                                parameters));
  }

  @Override
  public Camera.ShutterCallback getShutterCallback() {
    return(null);
  }

  @Override
  public void handleException(Exception e) {
    Log.e(getClass().getSimpleName(),
          "Exception in setPreviewDisplay()", e);
  }

  @Override
  public boolean mirrorFFC() {
    return(mirrorFFC);
  }

  @Override
  public void saveImage(PictureTransaction xact, byte[] image) {
    File photo=getPhotoPath();

    if (photo.exists()) {
      photo.delete();
    }

    try {
      FileOutputStream fos=new FileOutputStream(photo.getPath());
      BufferedOutputStream bos=new BufferedOutputStream(fos);

      bos.write(image);
      bos.flush();
      fos.getFD().sync();
      bos.close();

      if (scanSavedImage()) {
        MediaScannerConnection.scanFile(ctxt,
                                        new String[] { photo.getPath() },
                                        SCAN_TYPES, null);
      }
    }
    catch (java.io.IOException e) {
      handleException(e);
    }
  }

  @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
  @Override
  public void onAutoFocus(boolean success, Camera camera) {
    if (success
        && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
      new MediaActionSound().play(MediaActionSound.FOCUS_COMPLETE);
    }
  }

  @Override
  public boolean useSingleShotMode() {
    return(useSingleShotMode);
  }

  @Override
  public void autoFocusAvailable() {
    // no-op
  }

  @Override
  public void autoFocusUnavailable() {
    // no-op
  }


  @Override
  public boolean useFullBleedPreview() {
    return(useFullBleedPreview);
  }

  @Override
  public float maxPictureCleanupHeapUsage() {
    return(1.0f);
  }
  
  protected File getPhotoPath() {
    File dir=getPhotoDirectory();

    dir.mkdirs();

    return(new File(dir, getPhotoFilename()));
  }

  protected File getPhotoDirectory() {
    if (photoDirectory == null) {
      initPhotoDirectory();
    }

    return(photoDirectory);
  }

  private void initPhotoDirectory() {
    photoDirectory=
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
  }

  protected String getPhotoFilename() {
    String ts=
        new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());

    return("Photo_" + ts + ".jpg");
  }

  protected boolean useFrontFacingCamera() {
    return(useFrontFacingCamera);
  }

  protected boolean scanSavedImage() {
    return(scanSavedImage);
  }

  public static class Builder {
    private SimpleCameraHost host=null;

    public Builder(SimpleCameraHost host) {
      this.host=host;
    }

    public SimpleCameraHost build() {
      return(host);
    }

    public Builder useFullBleedPreview(boolean useFullBleedPreview) {
      host.useFullBleedPreview=useFullBleedPreview;

      return(this);
    }
  }
}
