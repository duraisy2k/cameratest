package com.freshfase.cameratest.util;

import android.hardware.Camera;
import android.media.MediaRecorder;
import android.view.View;

import java.io.IOException;

/**
 * Created by durai on 29/5/16.
 */
public interface PreviewStrategy {
  void attach(Camera camera) throws IOException;

  void attach(MediaRecorder recorder);

  View getWidget();
}