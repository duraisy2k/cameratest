package com.freshfase.cameratest.util;

import android.hardware.Camera;

/**
 * Created by durai on 29/5/16.
 */
public interface CameraHost extends Camera.AutoFocusCallback {

  Camera.Parameters adjustPictureParameters(PictureTransaction xact, Camera.Parameters parameters);

  Camera.Parameters adjustPreviewParameters(Camera.Parameters parameters);

  void autoFocusAvailable();

  void autoFocusUnavailable();

  int getCameraId();

  DeviceProfile getDeviceProfile();

  Camera.Size getPictureSize(PictureTransaction xact, Camera.Parameters parameters);

  Camera.Size getPreviewSize(int displayOrientation, int width,
                             int height, Camera.Parameters parameters);

  Camera.ShutterCallback getShutterCallback();

  void handleException(Exception e);

  boolean mirrorFFC();

  void saveImage(PictureTransaction xact, byte[] image);

  boolean useSingleShotMode();

  boolean useFullBleedPreview();
  
  float maxPictureCleanupHeapUsage();
}
