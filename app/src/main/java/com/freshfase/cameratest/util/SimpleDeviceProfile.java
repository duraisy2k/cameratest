package com.freshfase.cameratest.util;

import android.annotation.SuppressLint;
import android.os.Build;

import org.xmlpull.v1.XmlPullParser;


/**
 * Created by durai on 29/5/16.
 */
public class SimpleDeviceProfile extends DeviceProfile {
    private boolean useTextureView =
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                    && !isCyanogenMod();
    private boolean portraitFFCFlipped = false;
    private int minPictureHeight = 0;
    private int maxPictureHeight = Integer.MAX_VALUE;
    private boolean doesZoomActuallyWork = true;
    private boolean useDeviceOrientation = false;
    private int pictureDelay = 0;

    SimpleDeviceProfile load(XmlPullParser xpp) {
        StringBuilder buf = null;

        try {
            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (xpp.getEventType()) {
                    case XmlPullParser.START_TAG:
                        buf = new StringBuilder();
                        break;

                    case XmlPullParser.TEXT:
                        if (buf != null) {
                            buf.append(xpp.getText());
                        }
                        break;

                    case XmlPullParser.END_TAG:
                        if (buf != null) {
                            set(xpp.getName(), buf.toString().trim());
                        }
                        break;
                }

                xpp.next();
            }
        } catch (Exception e) {
        }

        return (this);
    }

    @SuppressLint("DefaultLocale")
    private void set(String name, String value) {
        if ("useTextureView".equals(name)) {
            useTextureView = Boolean.parseBoolean(value);
        } else if ("portraitFFCFlipped".equals(name)) {
            portraitFFCFlipped = Boolean.parseBoolean(value);
        } else if ("doesZoomActuallyWork".equals(name)) {
            doesZoomActuallyWork = Boolean.parseBoolean(value);
        } else if ("useDeviceOrientation".equals(name)) {
            useDeviceOrientation = Boolean.parseBoolean(value);
        } else if ("minPictureHeight".equals(name)) {
            minPictureHeight = Integer.parseInt(value);
        } else if ("maxPictureHeight".equals(name)) {
            maxPictureHeight = Integer.parseInt(value);
        }
        else if ("pictureDelay".equals(name)) {
            pictureDelay = Integer.parseInt(value);
        }
    }

    @Override
    public boolean useTextureView() {
        return (useTextureView);
    }

    @Override
    public boolean portraitFFCFlipped() {
        return (portraitFFCFlipped);
    }

    @Override
    public int getMinPictureHeight() {
        return (minPictureHeight);
    }

    @Override
    public int getMaxPictureHeight() {
        return (maxPictureHeight);
    }

    @Override
    public boolean doesZoomActuallyWork(boolean isFFC) {
        return (doesZoomActuallyWork);
    }

    @Override
    public int getPictureDelay() {
        return (pictureDelay);
    }


    private boolean isCyanogenMod() {
        return (System.getProperty("os.version").contains("cyanogenmod") || Build.HOST.contains("cyanogenmod"));
    }

    static class MotorolaRazrI extends SimpleDeviceProfile {
        public boolean doesZoomActuallyWork(boolean isFFC) {
            return (!isFFC);
        }
    }
}
