package com.freshfase.cameratest;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

/**
 * Created by durai on 29/5/16.
 */
public class MainActivity extends Activity implements
        DemoCameraFragment.Contract {
    private static final String STATE_SELECTED_NAVIGATION_ITEM =
            "selected_navigation_item";
    private static final String STATE_SINGLE_SHOT = "single_shot";
    private static final String STATE_LOCK_TO_LANDSCAPE =
            "lock_to_landscape";
    private static final int CONTENT_REQUEST = 1337;
    private DemoCameraFragment std = null;
    private DemoCameraFragment ffc = null;
    private DemoCameraFragment current = null;
    private boolean hasTwoCameras = (Camera.getNumberOfCameras() > 1);
    private boolean singleShot = false;
    private boolean isLockedToLandscape = false;
    private static boolean defaultCam = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        current = DemoCameraFragment.newInstance(false);

        getFragmentManager().beginTransaction()
                .replace(R.id.container, current).commit();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if (hasTwoCameras) {
            if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
                getActionBar().setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
            }
        }

        setSingleShotMode(savedInstanceState.getBoolean(STATE_SINGLE_SHOT));
        isLockedToLandscape =
                savedInstanceState.getBoolean(STATE_LOCK_TO_LANDSCAPE);

        if (current != null) {
            current.lockToLandscape(isLockedToLandscape);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (hasTwoCameras) {
            outState.putInt(STATE_SELECTED_NAVIGATION_ITEM,
                    getActionBar().getSelectedNavigationIndex());
        }

        outState.putBoolean(STATE_SINGLE_SHOT, isSingleShotMode());
        outState.putBoolean(STATE_LOCK_TO_LANDSCAPE, isLockedToLandscape);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.main, menu);

        menu.findItem(R.id.landscape).setChecked(isLockedToLandscape);

        return (super.onCreateOptionsMenu(menu));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.landscape) {
            item.setChecked(!item.isChecked());
            current.lockToLandscape(item.isChecked());
            isLockedToLandscape = item.isChecked();
        } else if (item.getItemId() == R.id.camera_switch) {
            if (hasTwoCameras) {
                defaultCam = !defaultCam;
                if (defaultCam) {
                    if (std == null) {
                        std = DemoCameraFragment.newInstance(false);
                    }
                    current = std;
                } else {
                    if (ffc == null) {
                        ffc = DemoCameraFragment.newInstance(true);
                    }

                    current = ffc;
                }
            } else {
                if (std == null) {
                    std = DemoCameraFragment.newInstance(false);
                }
                current = std;
            }
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, current).commit();

            findViewById(android.R.id.content).post(new Runnable() {
                @Override
                public void run() {
                    current.lockToLandscape(isLockedToLandscape);
                }
            });
        }

        return (super.onOptionsItemSelected(item));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == CONTENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                // do nothing
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_CAMERA && current != null
                && !current.isSingleShotProcessing()) {
            current.takePicture();

            return (true);
        }

        return (super.onKeyDown(keyCode, event));
    }

    @Override
    public boolean isSingleShotMode() {
        return (singleShot);
    }

    @Override
    public void setSingleShotMode(boolean mode) {
        singleShot = mode;
    }
}
